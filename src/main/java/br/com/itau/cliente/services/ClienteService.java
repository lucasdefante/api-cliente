package br.com.itau.cliente.services;

import br.com.itau.cliente.exceptions.ClientNotFoundException;
import br.com.itau.cliente.models.Cliente;
import br.com.itau.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente consultarClientePorId(int id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent()){
            // PRINT PARA TESTE DO RIBBON
            System.out.println("Cliente consultado: " + clienteOptional.get().toString() + " às " + LocalTime.now());
            // PRINT PARA TESTE DO RIBBON
            return clienteOptional.get();
        } else {
            throw new ClientNotFoundException();
        }
    }
}
